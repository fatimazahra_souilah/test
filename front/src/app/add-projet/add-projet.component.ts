import { Component, OnInit } from '@angular/core';
import { Projet } from '../models/projet';
import { ProjetServiceService } from '../Service/projet/projet-service.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-projet',
  templateUrl: './add-projet.component.html',
  styleUrls: ['./add-projet.component.css']
})
export class AddProjetComponent implements OnInit {
projet:Projet={
  nom:"",
  date: new Date()
};
  constructor(private projetService:ProjetServiceService,private router:Router,private dialog:MatDialog,
    public dialogRef: MatDialogRef<AddProjetComponent>) { }

  ngOnInit() {
  }

  onSaveProject(form){
    if(form.valid){
      this.projetService.addProje(this.projet).subscribe(()=>{
        this.router.navigate(['/listProjet'])
        this.dialogRef.close();
        
      })
    }
  }
}
