import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiProjetComponent } from './edi-projet.component';

describe('EdiProjetComponent', () => {
  let component: EdiProjetComponent;
  let fixture: ComponentFixture<EdiProjetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdiProjetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
