import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  projets;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private http:HttpClient
    ) {}
    ngOnInit() {  
      this.getBugs();
    }
  private getBugs()
  {
    this.http.get("http://localhost:9090/api/projets/")
        .subscribe(data=>{
            this.projets=data;
        },err=>{
          console.log(err);
        })
  }

}
