import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridProjetComponent } from './content/grid-projet/grid-projet.component';
import { GridUserComponent } from './content/grid-user/grid-user.component';
import { AddProjetComponent } from './add-projet/add-projet.component';
import { EdiProjetComponent } from './edi-projet/edi-projet.component';
import { BugsComponent } from './bugs/bugs.component';

const routes: Routes = [
  { path: '', redirectTo: 'listUtilisateur', pathMatch: 'full' },
  { path: 'listUtilisateur', component: GridUserComponent },
  { path: 'listProjet', component: GridProjetComponent },
  { path: 'projets/:id/bugs', component: BugsComponent, pathMatch: 'full' },
  {path: 'addProjet', component: AddProjetComponent },
  {path: 'editProjet', component: EdiProjetComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
