import { Component, OnInit } from '@angular/core';
import { AddProjetComponent } from 'src/app/add-projet/add-projet.component';
import { MatDialog } from '@angular/material';
import { ProjetServiceService } from 'src/app/Service/projet/projet-service.service';
import { Projet } from 'src/app/models/projet';
import { EdiProjetComponent } from 'src/app/edi-projet/edi-projet.component';

@Component({
  selector: 'app-grid-projet',
  templateUrl: './grid-projet.component.html',
  styleUrls: ['./grid-projet.component.css']
})
export class GridProjetComponent implements OnInit {
projets:Projet[];
idProjet:number;
projet:Projet;
nom:string;
page:number=1;

  constructor(private dialog: MatDialog,private projetService:ProjetServiceService) { }

  ngOnInit() {
    this.getAllProjets();
  }

  getAllProjets(){
    this.projetService.getAllProje().subscribe((projets:Projet[])=>{
      this.projets= projets;
          })
  }
  openDialog(): void {
    let dialogRef = this.dialog.open(AddProjetComponent);
  }

  openDialogEdit(id:number){
    let dialogRef = this.dialog.open(EdiProjetComponent,{
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.getAllProjets();
    })
  }

  deleteProjet(id){
    this.projetService.deleteProjes(id).subscribe(()=>{
    this.getAllProjets();
    })
  }
  Search(){
    if(this.nom !=""){
    this.projets=this.projets.filter(res=>{
    return res.nom.toLocaleLowerCase().match(this.nom.toLocaleLowerCase())
    });
    }else if(this.nom == ""){
    this.ngOnInit();
    }
    }
   
}
