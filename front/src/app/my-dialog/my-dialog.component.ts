import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { User } from '../models/user';

@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.css']
})
export class MyDialogComponent implements OnInit {
  
  @Output() addUserEvent: EventEmitter<any> = new EventEmitter()
  user: User;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: User
  ) {}

  ngOnInit() {  
    this.user = this.data;
  }

  onAddUser($event) {
    this.addUserEvent.emit($event);
  }

}
