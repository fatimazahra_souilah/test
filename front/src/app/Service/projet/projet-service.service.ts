import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Projet } from '../../models/projet';

@Injectable({
  providedIn: 'root'
})
export class ProjetServiceService {

  constructor(private http:HttpClient) { }
  getAllProje(){
    return this.http.get('http://localhost:9090/api/projets')
  }
  getOneProje(id:number){
    // return this.http.get('http://localhost:8081/api/projets/'+id)
    return this.http.get(`http://localhost:9090/api/projets/${id}`)
  }

  addProje(projet:Projet){
    return this.http.post('http://localhost:9090/api/projet',projet)
    
  }
  updateProjes(projet:Projet){
    return this.http.put('http://localhost:9090/api/projet',projet)
  }
  deleteProjes(id:number){
    return this.http.delete(`http://localhost:9090/api/projets/${id}`,{responseType:'text'})
  }
}
