import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BugService } from '../Service/Bug/bug.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-bugs',
  templateUrl: './bugs.component.html',
  styleUrls: ['./bugs.component.css']
})
export class BugsComponent implements OnInit {
  projetId;
  bugs;
  constructor(
    private activatedRoute: ActivatedRoute,
    private http:HttpClient
    ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(p => this.projetId = p['id'])
    this.getBugs(this.projetId)
  }

  getBugs(projetId)
  {
    this.http.get("http://localhost:9090/api/projets/" + this.projetId +"/bug")
        .subscribe(data=>{
            this.bugs=data;
        },err=>{
          console.log(err);
        })
  }

}
