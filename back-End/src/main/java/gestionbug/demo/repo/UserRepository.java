package gestionbug.demo.repo;

import gestionbug.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {

    List<User> findBynom(String nom);

}
