package gestionbug.demo.repo;

import gestionbug.demo.domain.Description;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DescriptionRepository extends JpaRepository<Description,Long> {
    Description findByIdD(Long Idb);
    List<Description>findByMessage(String message);
}
