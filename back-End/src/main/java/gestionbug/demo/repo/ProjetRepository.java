package gestionbug.demo.repo;

import gestionbug.demo.domain.Projet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200")
public interface ProjetRepository extends JpaRepository<Projet,Long> {


    List<Projet> findBynom(String nom);
}
